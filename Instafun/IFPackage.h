//
//  IFPackage.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IFPackage : NSManagedObject

@property (nonatomic, retain) NSString * packageId;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSNumber * downloaded;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * parsed;

@end

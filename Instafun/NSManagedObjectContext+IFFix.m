//
//  NSManagedObjectContext+IFFix.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "NSManagedObjectContext+IFFix.h"

@implementation NSManagedObjectContext (IFFix)


- (BOOL)saveToPersistentStore:(NSError **)error
{
    __block NSError *localError = nil;
    NSManagedObjectContext *contextToSave = self;
    while (contextToSave) {
        __block BOOL success;
        
        /**
         To work around issues in ios 5 first obtain permanent object ids for any inserted objects.  If we don't do this then its easy to get an `NSObjectInaccessibleException`.  This happens when:
         
         1. Create new object on main context and save it.
         2. At this point you may or may not call obtainPermanentIDsForObjects for the object, it doesn't matter
         3. Update the object in a private child context.
         4. Save the child context to the parent context (the main one) which will work,
         5. Save the main context - a NSObjectInaccessibleException will occur and Core Data will either crash your app or lock it up (a semaphore is not correctly released on the first error so the next fetch request will block forever.
         */
        [contextToSave obtainPermanentIDsForObjects:[[contextToSave insertedObjects] allObjects] error:&localError];
        if (localError) {
            if (error) *error = localError;
            return NO;
        }
        
        [contextToSave performBlockAndWait:^{
            success = [contextToSave save:&localError];
        }];
        
        if (! success) {
            if (error) *error = localError;
            return NO;
        }
        
        if (! contextToSave.parentContext && contextToSave.persistentStoreCoordinator == nil) {
            return NO;
        }
        contextToSave = contextToSave.parentContext;
    }
    
    return YES;
}

@end

//
//  IFDownloadPackagesOperation.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFDownloadRequestOperation.h>
#import "IFPackage.h"
#import "IFParsePackageOperation.h"

@class IFDownloadPackagesOperation;
@protocol IFDownloadPackagesOperationDelegate <NSObject>

- (void)noNewItems;
- (void)packagesDownloadCompleted;
- (void)packagesDownloadFailedWithError:(NSError *)error;
- (void)packageParsed:(NSUInteger)packageIndex;
- (void)package:(NSUInteger)packageIndex failedToParse:(NSError *)error;

- (void)updateOperationsProgress:(NSUInteger)completed
                 totalOperations:(NSUInteger)total;

- (void)updateProgressForPackage:(NSUInteger)packageIndex
                  totalBytesRead:(long long)totalBytesRead
                totalBytesToRead:(long long)totalBytesToRead;

@end

@interface IFDownloadPackagesOperation : NSOperation

- (id)initWithPackages:(NSArray *)packages andDelegate:(id<IFDownloadPackagesOperationDelegate>)aDelegate;

@end

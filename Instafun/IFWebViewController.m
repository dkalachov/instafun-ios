//
//  IFWebViewController.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFWebViewController.h"

@interface IFWebViewController ()

@property UIWebView *view;
@property UIBarButtonItem *item;

@end

@implementation IFWebViewController

+ (IFWebViewController *)webViewForHtml:(NSString *)html pageIndex:(NSInteger)index
{
    IFWebViewController *vc = [[IFWebViewController alloc] init];
    vc.pageIndex = index;
    [vc.view loadHTMLString:html baseURL:nil];
    return vc;
}

- (void)loadView
{
    self.view = [[UIWebView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
}

@end

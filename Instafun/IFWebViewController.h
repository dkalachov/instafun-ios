//
//  IFWebViewController.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IFBrowseViewController.h"

@interface IFWebViewController : UIViewController

+ (IFWebViewController *)webViewForHtml:(NSString *)html pageIndex:(NSInteger)index;
@property NSInteger pageIndex;
@property (weak) IFBrowseViewController *browser;

@end

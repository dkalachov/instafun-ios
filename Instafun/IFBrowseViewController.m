//
//  IFBrowseViewController.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFBrowseViewController.h"
#import "IFClient.h"
#import "IFPost+Own.h"
#import "IFWebViewController.h"
#import "IFAppDelegate.h"

@interface IFBrowseViewController ()

@property NSArray *posts;
@property NSInteger currentIndex;

@end

@implementation IFBrowseViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.posts = [[IFClient sharedClient] allPosts];
    if (!self.posts.count) {
        return;
    }
    self.dataSource = self;
    self.delegate = self;
    self.currentIndex = 0;
    [self setViewControllers:@[ [IFWebViewController webViewForHtml:[self htmlForIndex:0] pageIndex:0] ] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"~>" style:UIBarButtonItemStyleBordered target:self action:@selector(showNextPage)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        
    if (!self.posts.count) {
        [self.navigationController popViewControllerAnimated:NO];
        return;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem.enabled = self.posts.count && self.currentIndex != self.posts.count - 1;
}

- (NSString *)htmlForIndex:(NSInteger)index
{
    IFPost *psto = (IFPost *) [self.posts objectAtIndex:index];
    psto.read = @YES;
    [appDelegate.mainQueuedObjectContext saveToPersistentStore:nil];
    [[IFClient sharedClient] updateItemsCount];
    return [psto content];
}

- (void)showNextPage
{
    if (self.currentIndex + 1 >= self.posts.count) {
        return;
    }
    self.currentIndex++;
    IFBrowseViewController * weakSelf = self;
    [self setViewControllers:@[ [IFWebViewController webViewForHtml:[self htmlForIndex:self.currentIndex] pageIndex:self.currentIndex] ] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        if (weakSelf.currentIndex == weakSelf.posts.count -1) {
            weakSelf.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(IFWebViewController *)viewController
{
    NSInteger index = viewController.pageIndex + 1;
    if (index == self.posts.count) {
        return nil;
    }
    self.currentIndex = index;
    IFWebViewController *vc = [IFWebViewController webViewForHtml:[self htmlForIndex:index] pageIndex:index];
    
    return vc;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(IFWebViewController *)viewController
{
    NSInteger index = viewController.pageIndex - 1;
    if (index == -1) {
        return nil;
    }
    self.currentIndex = index;
    IFWebViewController *vc = [IFWebViewController webViewForHtml:[self htmlForIndex:index] pageIndex:index];
    return vc;
}

@end

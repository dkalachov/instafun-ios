//
//  IFParsePackageOperation.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IFPackage+Own.h"

@interface IFParsePackageOperation : NSOperation{
    BOOL executing;
    BOOL finished;
}

- (id)initWithPackage:(IFPackage *)package;
@property (strong, readonly, nonatomic) IFPackage *package;
@property dispatch_queue_t parseQueue;
@property (strong) NSError *error;
@property BOOL success;

@end

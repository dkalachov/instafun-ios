//
//  IFParsePackageOperation.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFParsePackageOperation.h"
#import "IFPost+Own.h"
#import "IFAppDelegate.h"
#import "IFClient.h"

@interface IFParsePackageOperation ()

@property (strong) NSManagedObjectContext *insertObjectContext;

@end

@implementation IFParsePackageOperation

- (id)initWithPackage:(IFPackage *)package
{
    self = [self init];
    if (self) {        
        _package = package;
        self.insertObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        self.insertObjectContext.parentContext = [appDelegate mainQueuedObjectContext];
    }
    return self;
}

#pragma mark - Overrides

- (void)main
{
    @try {               
        if ([self.package isParsable]) {
            [self parsePackage];
        }
        
        [self completeOperation];
    }
    @catch(...) {
        // Do not rethrow exceptions.
    }
}

- (void)start
{    
    // Always check for cancellation before launching the task.
    if ([self isCancelled]) {
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    dispatch_async(self.parseQueue, ^{
        [self main];
    });
    
//    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isFinished
{
    return finished;
}

#pragma mark - Internal

- (void)parsePackage
{
    __block NSError *error;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:self.package.extractedfileNamePath] options:NSJSONReadingAllowFragments error:&error];
    
    if (error) {
        self.error = error;
        return;
    }
    
    if ([self isCancelled]) return;
    
    if ([jsonObject respondsToSelector:@selector(objectForKey:)]) {
        [self.insertObjectContext performBlockAndWait:^{
            NSArray *posts = [jsonObject objectForKey:@"posts"];
            for (NSDictionary *post in posts) {
                NSString *postId = [post objectForKey:@"id"];
                
                IFPost *postManagedObject;
                // find existing object
                NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:@"IFPost"];
                fr.predicate = [NSPredicate predicateWithFormat:@"postId = %@", postId];
                postManagedObject = [[self.insertObjectContext executeFetchRequest:fr error:&error] lastObject];
                // or create new
                postManagedObject = postManagedObject?:[NSEntityDescription insertNewObjectForEntityForName:@"IFPost" inManagedObjectContext:self.insertObjectContext];
                
                postManagedObject.content = [post objectForKey:@"content"];
                postManagedObject.date = [NSDate dateWithTimeIntervalSince1970:[[post objectForKey:@"date"] integerValue]];
                postManagedObject.postId = postId;
                postManagedObject.title = [post objectForKey:@"title"];
                postManagedObject.metadata = [post objectForKey:@"metadata"];
            }
        }];
        
        [self.insertObjectContext saveToPersistentStore:&error];
        self.error = error;
        self.success = error == nil;
    } else {
        NSLog(@"What is %@", jsonObject);
    }
    
    if (self.success) {        
        self.package.parsed = @YES;
    }
}

- (void)deletePostWithId:(NSString *)postId
{
    
}

- (void)completeOperation
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end

//
//  IFAppDelegate.h
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSManagedObjectContext+IFFix.h"

@interface IFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSManagedObjectContext *mainQueuedObjectContext;
@property (strong, nonatomic) NSManagedObjectContext *persistentObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

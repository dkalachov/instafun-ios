//
//  IFPost.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFPost.h"


@implementation IFPost

@dynamic mediaType;
@dynamic content;
@dynamic date;
@dynamic postId;
@dynamic read;
@dynamic title;
@dynamic metadata;

@end

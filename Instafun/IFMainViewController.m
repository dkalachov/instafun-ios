//
//  IFMainViewController.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFMainViewController.h"
#import "IFClient.h"
#import "IFAppDelegate.h"

static void *IFUnreadItemsContext = &IFUnreadItemsContext;

@interface IFMainViewController ()

@property (weak) IFClient *client;
@property (strong, nonatomic) NSMutableArray *downloadProgressDict;
@property NSInteger completedPacks;
@property NSInteger totalPacks;

@end

@implementation IFMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back"]];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:NULL]];
    
    self.client = [IFClient sharedClient];
    [self.client addObserver:self forKeyPath:@"unreadItemsCount" options:NSKeyValueObservingOptionNew context:IFUnreadItemsContext];
    
    // remove start controller
    [self.navigationController setViewControllers:@[[self.navigationController.viewControllers lastObject]] animated:NO];
    
    [self setDefaultState];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.client removeObserver:self forKeyPath:@"unreadItemsCount"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == IFUnreadItemsContext) {
        [self updateInfo];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Actions

- (void)updateInfo
{
    if ([self isViewLoaded]) {
        NSUInteger unreadItemsCount = [self.client unreadItemsCount];
        self.browseInfoText.text = [NSString stringWithFormat:@"Unread items: %d", unreadItemsCount];
    }
}

- (void)setProgressContainerHidden:(BOOL)hidden
{
    self.progressContainer.hidden = hidden;
    if (hidden) {
        self.downloadProgressLabel.text = @"";
    }
}

- (IBAction)getPackages:(id)sender
{
    [self prepareForDownload];
    [self appendMessage:@"Searching for new content..."];
    [self.client getNewPackagesList:^(NSOperation *operation, NSArray *result) {
        if ([result count]) {
            [self setProgressContainerHidden:NO];
            [self appendMessage:@"Downloading..."];
            self.downloadProgressDict = [NSMutableArray arrayWithCapacity:result.count];
            for (NSInteger i = 0; i < result.count; i++) {
                [self.downloadProgressDict setObject:@{
                 @"total":@(-1), @"read":@0
                 } atIndexedSubscript:i];
            }
            
            [self.client downloadPackagesWithDelegate:self];
        } else {
            [self appendMessage:@"No new items!"];
            [self setProgressContainerHidden:YES];
        }
    } failure:^(NSOperation *operation, NSError *error) {
        [self appendMessage:[error description]];
    }];
}

#pragma mark

- (void)setDefaultState
{    
    self.statusText.text = @"";
    self.downloadProgress.progress = 0.0;
    [self setProgressContainerHidden:YES];
    [self updateInfo];
}

- (void)prepareForDownload
{
    self.totalPacks = -1;
    self.completedPacks = 0;
    [self updateProgressCommon];
}

- (void)appendMessage:(NSString *)message
{
    self.statusText.text = [NSString stringWithFormat:@"%@\n%@", self.statusText.text?:@"", message];
    CGPoint bottomOffset = CGPointMake(0, self.statusText.contentSize.height - self.statusText.bounds.size.height);
    [self.statusText setContentOffset:bottomOffset animated:YES];
}

#pragma mark - IFDownload Delegate

- (void)packagesDownloadFailedWithError:(NSError *)error
{
//    [self appendMessage:[NSString stringWithFormat:@"packagesDownloadFailedWithError %@", error]];
}

- (void)packagesDownloadCompleted
{
//    [self appendMessage:[NSString stringWithFormat:@"packagesDownloadCompleted"]];
}

- (void)updateOperationsProgress:(NSUInteger)completed totalOperations:(NSUInteger)total
{
    self.completedPacks = completed;
    self.totalPacks = total;
    [self updateProgressCommon];
}

- (void)updateProgressForPackage:(NSUInteger)packageIndex totalBytesRead:(long long)totalBytesRead totalBytesToRead:(long long)totalBytesToRead
{
    [self.downloadProgressDict setObject:
     @{@"total":@(totalBytesToRead),
     @"read":@(totalBytesRead)}
                      atIndexedSubscript:packageIndex];
    [self updateProgressCommon];
}

- (void)noNewItems
{
    [self appendMessage:[NSString stringWithFormat:@"No new items!"]];
    [self setProgressContainerHidden:YES];    
}

#pragma mark

- (void)updateProgressCommon
{
    NSArray *totals = [self.downloadProgressDict valueForKey:@"total"];
    NSString *size = @"";
    if ([totals indexOfObject:@(-1)] == NSNotFound) {
        long long total = [self sumOf:totals];
        long long read = [self sumOf:[self.downloadProgressDict valueForKey:@"read"]];
        
        self.downloadProgress.progress = ((float)read)/total;
        size = [NSString stringWithFormat:@" (%.2f mb)", (float)total / (1024 * 1024)];
    } else {
        self.downloadProgress.progress = 0.0;
    }
    self.downloadProgressLabel.text = self.totalPacks > 0 ? [NSString stringWithFormat:@"%d of %d%@", self.completedPacks, self.totalPacks, size] : @"";
    if (self.completedPacks == self.totalPacks) {
        [self appendMessage:@"Done!"];
    }
}

- (long long)sumOf:(NSArray *)array
{
    long long r = 0;
    for (NSNumber *n in array) {
        r += [n longLongValue];
    }
    return r;
}

- (void)packageParsed:(NSUInteger)packageIndex
{
    [[IFClient sharedClient] updateItemsCount];
}

- (void)package:(NSUInteger)packageIndex failedToParse:(NSError *)error
{
    NSLog(@"packagefailedToParse %d %@", packageIndex, error);
}

@end

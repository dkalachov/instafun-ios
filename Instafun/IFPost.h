//
//  IFPost.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IFPost : NSManagedObject

@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * postId;
@property (nonatomic, retain) NSNumber * read;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) id metadata;

@end

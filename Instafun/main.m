//
//  main.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IFAppDelegate class]));
    }
}

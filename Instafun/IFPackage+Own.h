//
//  IFPackage+Own.h
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFPackage.h"

@interface IFPackage (Own)

- (BOOL)isParsable;
- (NSString *)fileNamePath;
- (NSString *)extractedfileNamePath;
- (NSString *)extractDirPath;

@end

//
//  IFPackage.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFPackage.h"


@implementation IFPackage

@dynamic packageId;
@dynamic date;
@dynamic downloaded;
@dynamic url;
@dynamic parsed;

@end

//
//  IFBrowseViewController.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFBrowseViewController : UIPageViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

- (void)showNextPage;

@end

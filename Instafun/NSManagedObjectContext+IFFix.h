//
//  NSManagedObjectContext+IFFix.h
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (IFFix)

- (BOOL)saveToPersistentStore:(NSError **)error;

@end

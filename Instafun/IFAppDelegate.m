//
//  IFAppDelegate.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFAppDelegate.h"
#import "IFClient.h"
#import <AFHTTPRequestOperationLogger.h>

@implementation IFAppDelegate

@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupUserDefaults];
    [self setupNetworking];
    [self setupAppearance];
    [self createContexts];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark
- (void)setupNetworking
{    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    if (YES) {
        [[AFHTTPRequestOperationLogger sharedLogger] startLogging];
        [[AFHTTPRequestOperationLogger sharedLogger] setLevel:AFLoggerLevelError];
    }
}

#pragma mark - User interfaces

- (void)setupAppearance
{
    UIColor *color = [UIColor colorWithRed:.0 green:40.0/255 blue:.0 alpha:1.0];
    [[UINavigationBar appearance] setTintColor:color];
    [[UIBarButtonItem appearance] setTintColor:color];
}

#pragma mark - User settings

- (void)setupUserDefaults
{    
    [userDefs registerDefaults:@{
                  kFirstLaunch:@YES,
         kPackagesDownloadSize:@10,
                   kApiBaseUrl:@"http://test.cleverbag.ru/api/",
                   kApiVersion:@"", // for future uses
     }];
    
    [userDefs synchronize];
}

#pragma mark - Core Data stack

- (void)saveContext
{
    NSError *error;
    [self.mainQueuedObjectContext saveToPersistentStore:&error];
}

- (void)createContexts
{
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    self.persistentObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    self.persistentObjectContext.persistentStoreCoordinator = coordinator;
    self.mainQueuedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    self.mainQueuedObjectContext.parentContext = self.persistentObjectContext;    
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Instafun" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Instafun.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
        
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }

    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

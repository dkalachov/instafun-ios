//
//  IFClient.h
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "IFDownloadPackagesOperation.h"

typedef void(^IFRequestSuccessBlock)(NSOperation *operation, id result);
typedef void(^IFRequestFailureBlock)(NSOperation *operation, NSError *error);

extern NSString *kApiBaseUrl;
extern NSString *kApiVersion;
extern NSString *kFirstLaunch;
extern NSString *kAuthToken;
extern NSString *IFErrorDomain;
extern NSString *kIFErrorResponse;
extern NSString *kPackagesDownloadSize;

enum IFErrorCodes {
    IFErrorCodeNoAuthToken = 1,    
    IFErrorCodeUnzipOpen = 2,
    IFErrorCodeUnzip = 3,
    };

@interface IFClient : AFHTTPClient

@property (strong, nonatomic) NSDate *lastSync;

// Shared instance
+ (IFClient *)sharedClient;
+ (BOOL)changeBaseUrl:(NSString *)newUrl;
+ (NSArray *)supportedAPIVersions;

// Configuration
@property (nonatomic) NSUInteger packagesDownloadSize;

// User API
- (BOOL)hasAuthToken;
- (void)clearAuthToken;
- (void)getAuthToken:(IFRequestSuccessBlock)success failure:(IFRequestFailureBlock)failure;

// Packages API
- (void)getNewPackagesList:(IFRequestSuccessBlock)success failure:(IFRequestFailureBlock)failure;
- (IFDownloadPackagesOperation *)downloadPackagesWithDelegate:(id<IFDownloadPackagesOperationDelegate>)delegate;

// Local API
@property NSUInteger unreadItemsCount;
- (void)updateItemsCount;
- (NSArray *)allPosts;

// Parse packages with parsed=0 and downloaded=1
- (void)parseDownloadedPackages;


@end

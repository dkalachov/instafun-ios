//
//  IFPackage+Own.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFPackage+Own.h"
#import "IFAppDelegate.h"
#import "IFPost+Own.h"

@implementation IFPackage (Own)

- (BOOL)isParsable
{
    return [self.downloaded boolValue]
//    && ![self.parsed boolValue]
    && [[NSFileManager defaultManager] fileExistsAtPath:self.extractedfileNamePath];
}

- (NSString *)extractDirPath
{
    NSString *packagePath = [NSString stringWithFormat:@"packages-%@", self.packageId];
//    return packagePath;
    return [[[appDelegate applicationDocumentsDirectory] path] stringByAppendingPathComponent:packagePath];
}

- (NSString *)extractedfileNamePath
{
    return [[self extractDirPath] stringByAppendingPathComponent:@"manifest.json"];
}

- (NSString *)fileNamePath
{
    NSString *packagePath = [NSString stringWithFormat:@"packages-%@.pkg", self.packageId];
//    return packagePath;
    return [[[appDelegate applicationDocumentsDirectory] path] stringByAppendingPathComponent:packagePath];
}

@end

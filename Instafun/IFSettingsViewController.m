//
//  IFSettingsViewController.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFSettingsViewController.h"
#import "IFClient.h"

@interface IFSettingsViewController ()

@end

@implementation IFSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.apiUrl.text = [userDefs stringForKey:kApiBaseUrl];
    self.packageSize.text = [NSString stringWithFormat:@"%d", [[IFClient sharedClient] packagesDownloadSize]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.apiUrl) {
        
        BOOL success = [IFClient changeBaseUrl:textField.text];
        [[[UIAlertView alloc] initWithTitle:success ? @"Success!" : @"Fail!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return success;
        
    } else if (textField == self.packageSize) {
        
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        NSUInteger newValue = [[nf numberFromString:self.packageSize.text] integerValue];
        if (newValue == 0) {
            return NO;
        }
        
        [[IFClient sharedClient] setPackagesDownloadSize:newValue];
    }
    
    [textField resignFirstResponder];
    return YES;
}

@end

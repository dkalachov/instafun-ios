//
//  IFSettingsViewController.h
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFSettingsViewController : UITableViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *apiUrl;
@property (weak, nonatomic) IBOutlet UITextField *packageSize;

@end

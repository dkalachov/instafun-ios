//
//  IFClient.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFClient.h"
#import "IFAppDelegate.h"
#import "IFPackage+Own.h"
#import "IFPost+Own.h"

NSString *kApiBaseUrl = @"IFApiBaseUrl";
NSString *kApiVersion = @"IFAPIVersion";
NSString *kAuthToken = @"IFAuthToken";
NSString *kFirstLaunch = @"IFFirstLaunch";
NSString *IFErrorDomain = @"IFErrorDomain";
NSString *kIFErrorResponse = @"response";
NSString *kLastSyncDate = @"lastSync";
NSString *kPackagesDownloadSize = @"downSize";

static IFClient *sharedClient;

@interface IFClient ()

@property NSManagedObjectContext *insertContext;

@end

@implementation IFClient

+ (IFClient *)sharedClient
{
    if (!sharedClient) {
        [self resetClient];
    }
    
    NSAssert(sharedClient, @"No shared client was set!");    
    return sharedClient;
}

+ (void)resetClient
{
    sharedClient = [[self alloc] initWithAPIURL:[userDefs stringForKey:kApiBaseUrl] version:[userDefs stringForKey:kApiVersion]];
}

+ (NSArray *)supportedAPIVersions
{
    return @[ @"" ];
}

+ (BOOL)changeBaseUrl:(NSString *)newUrl
{
    
    if (![NSURL URLWithString:newUrl]) {
        // not url
        return NO;
    }
    
    [userDefs setObject:newUrl forKey:kApiBaseUrl];
    [userDefs synchronize];
    
    [IFClient resetClient];
    return sharedClient != nil;
}

- (id)initWithAPIURL:(NSString *)url version:(NSString *)version
{
    NSString *apiUrlString = [NSString stringWithFormat:@"%@%@", url, version];
    NSURL *apiUrl = [NSURL URLWithString:apiUrlString];
    if ((self = [self initWithBaseURL:apiUrl])) {
        self.lastSync = [userDefs objectForKey:kLastSyncDate];
        self.unreadItemsCount = [self unreadItemsCountFromCoreData];
        self.insertContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        self.insertContext = appDelegate.mainQueuedObjectContext;
        
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        if ([self hasAuthToken]) {
            [self setAuthorizationHeaderWithToken:[userDefs stringForKey:kAuthToken]];
        }
    }
    return self;
}

#pragma mark - Custom get&set

- (void)setPackagesDownloadSize:(NSUInteger)packagesDownloadSize
{
    [userDefs setInteger:packagesDownloadSize forKey:kPackagesDownloadSize];
    [userDefs synchronize];
}

- (NSUInteger)packagesDownloadSize
{
    return [userDefs integerForKey:kPackagesDownloadSize];
}

#pragma mark - Token
- (BOOL)hasAuthToken
{
    return [userDefs stringForKey:kAuthToken] != nil;
}

- (void)clearAuthToken
{
    [userDefs removeObjectForKey:kAuthToken];
    [userDefs synchronize];
}

- (void)setAuthToken:(NSString *)authToken
{
    [userDefs setObject:authToken forKey:kAuthToken];
    [userDefs synchronize];
    [self setAuthorizationHeaderWithToken:authToken];
}

- (void)getAuthToken:(IFRequestSuccessBlock)success failure:(IFRequestFailureBlock)failure
{
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];    
	NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                      path:@"user"
                                                parameters:@{ @"udid":udid }];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject respondsToSelector:@selector(objectForKey:)]) {
            NSString *authToken = [responseObject objectForKey:@"token"];
            if (authToken && ![authToken isEqualToString:@""]) {
                [self setAuthToken:authToken];
                if (success) success(operation, authToken);
                return;
            }
        }
        
        if (failure) failure(operation,
                             [NSError errorWithDomain:IFErrorDomain
                                                 code:IFErrorCodeNoAuthToken
                                             userInfo:@{
                                     kIFErrorResponse:responseObject?:nilObj
                              }]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {        
        if (failure) failure(operation, error);
    }];
    
    [self enqueueHTTPRequestOperation:operation];
}

#pragma mark - Packages

- (id)getSyncData
{
    return self.lastSync?:nilObj;
}

- (void)getNewPackagesList:(IFRequestSuccessBlock)success failure:(IFRequestFailureBlock)failure
{
	NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                      path:@"packages"
                                                parameters:@{
                                    @"sync_data":[self getSyncData],
                                    @"size":@([self packagesDownloadSize]),
                                    }];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
	AFHTTPRequestOperation *operation =
    [self HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *packages = [responseObject objectForKey:@"packages"];
        __block NSError *error;
        
        for (id package in packages) {
            __block IFPackage *packageManagedObject;
            NSString *packageId = package;
            // find existing object
            NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:@"IFPackage"];
            fr.predicate = [NSPredicate predicateWithFormat:@"packageId = %@", packageId];
            [appDelegate.mainQueuedObjectContext performBlockAndWait:^{                
                packageManagedObject = [[appDelegate.mainQueuedObjectContext executeFetchRequest:fr error:&error] lastObject];
            }];
            
            // or create new
            [self.insertContext performBlockAndWait:^{
                packageManagedObject = packageManagedObject?:
                [NSEntityDescription
                 insertNewObjectForEntityForName:@"IFPackage"
                 inManagedObjectContext:self.insertContext];
            }];
            
            packageManagedObject.packageId = packageId;
            packageManagedObject.url =
            [[userDefs stringForKey:kApiBaseUrl]
             stringByAppendingString:
             [NSString stringWithFormat:@"package/%@", packageId]];
        }
        
        [self.insertContext saveToPersistentStore:&error];
        
        if (success) success(operation, packages);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) failure(operation, error);
    }];
    
    [self enqueueHTTPRequestOperation:operation];
}

- (IFDownloadPackagesOperation *)downloadPackagesWithDelegate:(id<IFDownloadPackagesOperationDelegate>)delegate
{
    NSError *error;
    NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:@"IFPackage"];
    fr.predicate = [NSPredicate predicateWithFormat:@"downloaded = 0"];
    NSArray *packages = [appDelegate.mainQueuedObjectContext executeFetchRequest:fr error:&error];
    if (error) {
        [delegate packagesDownloadFailedWithError:error];
        return nil;
    }
    
    if (packages.count == 0) {
        [delegate noNewItems];
        return nil;
    }
    
    IFDownloadPackagesOperation *downloadOp = [[IFDownloadPackagesOperation alloc] initWithPackages:packages andDelegate:delegate];
    [self.operationQueue addOperation:downloadOp];
    
    return downloadOp;
}

#pragma mark - Local

- (void)updateItemsCount
{
    self.unreadItemsCount = [self unreadItemsCountFromCoreData];
}

- (NSFetchRequest *)allUnreadPostsfetchRequest
{
    NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:@"IFPost"];
    fr.predicate = [NSPredicate predicateWithFormat:@"read = 0"];
    return fr;
}

- (NSArray *)allPosts
{
    NSError *error;
    NSArray *result = [appDelegate.mainQueuedObjectContext executeFetchRequest:[self allUnreadPostsfetchRequest] error:&error];
    return result;
}

- (NSUInteger)unreadItemsCountFromCoreData
{
    NSUInteger count = 0;
    NSError *error;
    count = [appDelegate.mainQueuedObjectContext countForFetchRequest:[self allUnreadPostsfetchRequest] error:&error];
    
    return error ? 0 : count;
}

@end

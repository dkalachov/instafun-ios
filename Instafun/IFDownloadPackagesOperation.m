//
//  IFDownloadPackagesOperation.m
//  Instafun
//
//  Created by Dima Kalachov on 19.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFDownloadPackagesOperation.h"
#import "IFClient.h"
#import <SSZipArchive.h>
#import "IFAppDelegate.h"

@interface IFDownloadPackagesOperation ()

@property dispatch_queue_t parseQueue;
@property NSMutableArray *downloadOperations;
@property (strong, nonatomic) NSArray *packages;
@property (weak, nonatomic) id<IFDownloadPackagesOperationDelegate> delegate;

@end

@implementation IFDownloadPackagesOperation

- (id)initWithPackages:(NSArray *)packages andDelegate:(id<IFDownloadPackagesOperationDelegate>)aDelegate
{
    if ((self = [self init])) {
        self.delegate = aDelegate;
        self.packages = packages;
        self.parseQueue = dispatch_queue_create("instafun.parse.queue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

#pragma mark - Overrides

- (void)cancel
{
    // propagate cancel message to all created operations
    [self.downloadOperations enumerateObjectsUsingBlock:^(NSOperation *op, NSUInteger idx, BOOL *stop) {
        [op cancel];
    }];
    
    self.packages = nil;
    self.parseQueue = nil;
    
    [super cancel];
}

- (void)main
{
    @try {        
        self.downloadOperations = [NSMutableArray arrayWithCapacity:self.packages.count];
        
        for (NSInteger i = 0; i < self.packages.count; i++) {
            if ([self isCancelled]) return;
            
            IFPackage *package = [self.packages objectAtIndex:i];
            
            // create download operation
            NSURLRequest *dRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:package.url]];
            AFDownloadRequestOperation *downloadPackageOp =
            [[AFDownloadRequestOperation alloc] initWithRequest:dRequest
                                                     targetPath:package.fileNamePath
                                                   shouldResume:NO];
            downloadPackageOp.shouldOverwrite = YES;
            
            [downloadPackageOp setProgressiveDownloadProgressBlock:^(AFDownloadRequestOperation *operation, NSInteger bytesRead, long long totalBytesRead, long long totalBytesExpected, long long totalBytesReadForFile, long long totalBytesExpectedToReadForFile) {
                [self.delegate updateProgressForPackage:i totalBytesRead:totalBytesRead totalBytesToRead:totalBytesExpected];
            }];
            
            [downloadPackageOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSError *unzipError;
                [SSZipArchive unzipFileAtPath:package.fileNamePath toDestination:package.extractDirPath overwrite:YES password:nil error:&unzipError];
                if (!unzipError) {                        
                    package.downloaded = @YES;
                    
                    IFParsePackageOperation *parseOperation = [[IFParsePackageOperation alloc] initWithPackage:package];
                    parseOperation.parseQueue = self.parseQueue;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
                    [parseOperation setCompletionBlock:^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (parseOperation.success) {
                                [self.delegate packageParsed:i];
                            } else {
                                [self.delegate package:i failedToParse:parseOperation.error];
                            }                            
                        });
                    }];
#pragma clang diagnostic pop
                    
                    [parseOperation start];
                } else {
                    [self.delegate package:i failedToParse:unzipError];
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSError *deleteError;
                [(AFDownloadRequestOperation *) operation deleteTempFileWithError:&deleteError];
#warning Handle in future
                // do nothing...
                [self.delegate packagesDownloadFailedWithError:error];
            }];
            
            [self.downloadOperations addObject:downloadPackageOp];
        }
        
        [[IFClient sharedClient] enqueueBatchOfHTTPRequestOperations:self.downloadOperations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
            [self.delegate updateOperationsProgress:numberOfFinishedOperations totalOperations:totalNumberOfOperations];
        } completionBlock:^(NSArray *operations) {
            [self.delegate packagesDownloadCompleted];
        }];
    }
    @catch(...) {
        // Do not rethrow exceptions.
    }
}

@end

//
//  IFStartViewController.m
//  Instafun
//
//  Created by Dima Kalachov on 18.05.13.
//  Copyright (c) 2013 Dima Kalachov. All rights reserved.
//

#import "IFStartViewController.h"

@interface IFStartViewController ()

@end

@implementation IFStartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back"]];
    
    if ([IFClient.sharedClient hasAuthToken]) {
        [self showMainViewController];
    } else {
        [self getAuthToken:self];
    }
}

#pragma mark

- (IBAction)getAuthToken:(id)sender
{
    [self showActivity];
    [IFClient.sharedClient getAuthToken:^(NSOperation *operation, id result) {
        if ([userDefs boolForKey:kFirstLaunch]) {
            // do something on first launch?
            
            
            [userDefs setBool:NO forKey:kFirstLaunch];
        }
        
        [self showMainViewController];
        
    } failure:^(NSOperation *operation, NSError *error) {
        [self hideActivityWithError:error];
    }];
}

- (void)showMainViewController
{
    [self performSegueWithIdentifier:@"DownloadPageSegue" sender:self];
}

#pragma mark

- (void)showActivity
{
    [self.retryButton setEnabled:NO];
    [self.retryButton setHidden:YES];
    [self.message setHidden:YES];
    [self.spinner startAnimating];
}

- (void)hideActivityWithError:(NSError *)error
{
    [self.spinner stopAnimating];
    [self.retryButton setEnabled:YES];
    [self.retryButton setHidden:NO];
    [self.message setHidden:NO];
    [self.message setText:[NSString stringWithFormat:@"%@", error]];
}

#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
